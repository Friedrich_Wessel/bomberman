﻿Lecture 4 

In der Scenen befinden sich der Würfel namens Player. 
Dieser kann wie folgt erstellt werden: 
* Create -> 3dObject -> Cube
* Den Cube auswählen und "Player" nennen 
* Player in der Hirachy selektieren, rechts im Inspector mit dem "AddComponent" Button folgende Elemente hinzufügen : 
	* Boxxcollider3d ( der existiert normalerweise schon - vorher checken ) 
		* Der BoxCollider muss im Material Slot den Material "NoFriction" verknüpft haben
	* Rigidbody 
	* Robot3d
* Das Robot Script steuert die Bewegung über die Tasten 
* Wenn sich der Roboter nicht bewegt ist wahrscheinlich der Speed zu klein ( Robot Componente ) 
	* ein Wert von 30 funktioniert gut 
	
Goodie kann erstellt werden : 
* Create -> 3dObject -> Cube 
* Den Cube "Goodie" nennen 
* Über den AddComponent Button folgendes Element hinzufügen : 
	* Goodie3d 
	* BoxCollider3d ( existiert wahrschienlich schon ) 
	* Auf dem BoxCollider den Haken bei "IsTrigger" setzen
		* Dadurch wird auf unserem Goodie Script die OnTriggerEnter aufgerufen wenn der Spieler mit der Box Kollidiert
	

