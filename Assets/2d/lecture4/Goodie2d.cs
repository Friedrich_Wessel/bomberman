﻿using UnityEngine;
using System.Collections;

public class Goodie2d : MonoBehaviour {

	/// <summary>
	/// Dieses Script wird von Unity aufgerufen wenn der BoxCollider von dem Object "Player"
	/// Mit dem Boxcollider von diesem Goodie kollidiert
	/// Der Collider der übergeben wird, ist das andere Boxcollider ( also der vom Player ) 
	/// </summary>
	void OnTriggerEnter2D(Collider2D collider){
		// wir wissen das der andere Collider der Robot ist, also holen 
		// wir uns die Komponente von diesem Objekt
		Robot2d robot = collider.GetComponent<Robot2d>(); 

		// Danach können die in Robot2d existierende Funktion CollectGoodie aufrufen 
		robot.CollectGoodie(); 

		// und das Goodie zertören
		// this.gameOject bezieht sich immer auf das Object auf dem das Script exisitiert
		Destroy( this.gameObject ) ; 
	}
}
