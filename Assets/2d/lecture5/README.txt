﻿Lecture 5 - 2D 


Diese Datei beschreibt den generellen Scenen Aufbau. Die Programmierung ist mit einzelnen Kommentaren an den Zeilen 
in den Script beschrieben. 
Anmerkung zu Mono und den Kommentaren: 
Links am Rand der Scripte ( da wo sich die Zeilennummern befinden ) sind manchmal kleine + zeichen zu sehen 
damit lassen sich kommentar blöcke ausklappen 


Testscene : Robot2D animated 
In der Scene befinden sich zwei Unterschiedliche Konzepte für einen animierten 2d Character 



Variante 1: Einzelbild Animationen 
Das Object: Player_Single_Frame animation ist ein Image auf dem das Sprite der Image Komponente animiert ist. 
Diese Animation ist durch den Animator ( ganz unten im Inspector ) kontrolliert. 
Das Script: Robot2d_withAnimation.cs schaltet die Animation ein und aus 

Variante 2: Object: Robot_Rig
 Animieren der einzelkomponenten in Unity 
Auf dem Spritesheet: Robot_Character_Sheet das sich im Ordner als PNG befindet
sind alle Körperteile des Roboters einzeln. Das Spritesheet wurde als Sprite 2d mit der Einstellung Multiple importiert 
( siehe lecture_notes lecture 1 https://incom.org/projekt/5784 ) 
Dann wurden verschiedene leer GameObjects erstellt und als Skelett benutzt ( also an die Position der Gelenke platziert ) 
Die Bilder wurden danach unter diese Objekte ge-parented ( hirarchisch darunter sortiert )
Dann kann man die Gelenk-Objekte mit normalen Unity Animationen animieren und erhält das Resultat wie auf dem Robot_Rig

Der restliche Aufbau entspricht Variante 1

Überblick zum Animator Controller : 
http://unity3d.com/learn/tutorials/modules/beginner/animation/animator-controller
Die Beispiele sind zwar mit 3d Elementen, die Konzepte funktionieren aber im 2d genauso 





