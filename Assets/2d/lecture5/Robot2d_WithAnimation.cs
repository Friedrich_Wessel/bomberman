﻿using UnityEngine;
using System.Collections;

public class Robot2d_WithAnimation : MonoBehaviour {

	// Dieses Script behinhaltet nur Kommentare zu den erweiterungen gegebnüber Robot2d
	// alle Funktionen die schon vorhanden waren befinden sich in Robot2d.cs

	// Wir benötigen die Animator komponente um die Animations Stati zu steuern 
	// Durch den Modifikator "public" erscheint die Variable im Inspector und wir 
	// können durch Drag & Drop verknüpfen 
	public Animator animator; 

	public float speed = 1; 
	
	void Update () {
		// Wir testen ob wir den Animations Status ändern müssen 
		// Da CheckAxis Funktion die Velocity auf 0 setzt ( in der Funktion MoveWithForces ) 
		// müssen wir zuerst die Animation testen 
		CheckAnimations(); 

		CheckAxis(); 
	}

	private void CheckAxis(){

		float horizontalInput = Input.GetAxis("Horizontal"); 


		Vector3 moveDirection = new Vector3(horizontalInput*speed,0,0); 
		MoveRobotWithForces( moveDirection ) ; 


		float verticalInput = Input.GetAxis("Vertical"); 
		moveDirection = new Vector3(0,verticalInput*speed,0); 
		MoveRobotWithForces( moveDirection ) ; 
	}

	private void CheckAnimations(){
		// Idee: Um zu enscheiden ob wir die Walk animation abspielen müssen 
		// testen wir ob die Geschwindigkeit des Characters in x richtung > 0 ist 
		// dafür müssen wir die Geschwindigkeit des Physik Objects des Characters auslesen 
		var player = GetComponent<Rigidbody2D>(); 

		if ( player.velocity.x > 0 ) {
			// Im Animator haben wir die Checkbox ( bool ) "IsWalking" eingestellt ( siehe README ) 
			// diese Checkbox schalten wir jetzt an, da sich unser Character vorwärts bewegt
			animator.SetBool("IsWalking", true); 
		}else {
			// Hier schalten wir die Checkbox ab, da unser Character steht ( oder Rückwärts geht ) 
			animator.SetBool("IsWalking", false); 
		}
	}
	

	void MoveRobotWithForces(Vector3 positionChange){

		var player = GetComponent<Rigidbody2D>(); 
		player.velocity = new Vector3( 0, 0, 0 ) ; 
		player.AddForce( positionChange*speed ) ; 
	}

	/// <summary>
	/// Diese Funktion wird von Goodie3d aufgerufen, wenn der Character in das Goodie läuft und damit
	/// die Funktion OnTriggerEnter auslöst.
	/// </summary>
	public void CollectGoodie(){
		Debug.Log("Yippih I got stuff");
	}


}
