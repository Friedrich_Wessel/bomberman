﻿Lecture 5 - 3d


Diese Datei beschreibt den generellen Scenen Aufbau. Die Programmierung ist mit einzelnen Kommentaren an den Zeilen 
in den Script beschrieben. 
Anmerkung zu Mono und den Kommentaren: 
Links am Rand der Scripte ( da wo sich die Zeilennummern befinden ) sind manchmal kleine + zeichen zu sehen 
damit lassen sich kommentar blöcke ausklappen 


Testscene : test3dRobot 

Robot ist ein animiertes 3d Objekt aus Blender exportiert als FBX 
Die Animationen wurden in den Importsettings eingestellt. 

Darauf befindet sich das RobotCharacter Script das die Animationen und Bewegung steuert
Die Animationsstates sind in dem Animation Controller: robot_anim_01 definiert ( auf dem Robot Animator verlinkt ) 
Die restlichen elemente sind nur good-looking environment Objects die als Hindernisse für den Roboter dienen ( aufgrund der 3d-Box-Collider )
Der grüne stein auf dem stein rechts vom Robot ( Object: Goodie ) ist das Goodie, das sich wie das Goodie3d verhält nur jezt mit dem RobotCharacter zusammen 
arbeiten kann.

Überblick zum Animator Controller : 
http://unity3d.com/learn/tutorials/modules/beginner/animation/animator-controller
Die Beispiele sind zwar mit 3d Elementen, die Konzepte funktionieren aber im 2d genauso 





