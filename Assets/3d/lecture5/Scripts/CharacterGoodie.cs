﻿using UnityEngine;
using System.Collections;

public class CharacterGoodie : MonoBehaviour {

	void OnTriggerEnter(Collider collider){
		RobotCharacter robot = collider.GetComponent<RobotCharacter>(); 
		robot.CollectGoodie(); 
		Destroy( this.gameObject ) ; 
	}
}
