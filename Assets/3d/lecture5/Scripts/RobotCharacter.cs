﻿using UnityEngine;
using System.Collections;

public class RobotCharacter : MonoBehaviour {

	// Dieses Script behinhaltet nur Kommentare zu den erweiterungen gegebnüber Robot2d
	// alle Funktionen die schon vorhanden waren befinden sich in Robot2d.cs
	public float MoveSpeed; 
	public float RotateSpeed; 
	public float JumpPower; 

	// Dieses Script benutzt im Gegensatz zu Robot3d keinen Rigidbody den wir mit Forces herumschieben 
	// sondern den Unity Character Controller
	// Der Vorteil an diesem Controller ist, das der Character nicht durch das herumschieben ausversehen 
	// umfallen kann, und trotzdem mit Triggern ( zum Beispiel dem Goodie ) und Wänden kollidert
	// der Nachteil ist, das der Character nicht mehr von der Schwerkraft beeinflusst wird - das müssen wir 
	// selber schreiben

	// Da wir den Character Controller andauernd brauchen, speichern wir ihn global für das ganze Script
	// damit wir ihn nicht jedesmal über GetComponent holen müssen 
	CharacterController character; 

	// mit hilfe dieser Bewegungsrichtung können wir die Schwerkraft wirkung selber programmieren 
	// die Richtung muss persistent über mehrere Frames bleiben, und muss deswegen hier global gespeichert werden 
	// anstatt in der Update immer wieder neu berechnet zu werden.
	Vector3 moveVertical; 

	// Den animator benötigen wir um zwischen den unterschiedlichen Animationen hin und her zu schalten 
	public Animator animator; 

	// Damit während der Jump Animation nicht andere Animationen gestartet werden, müssen wir uns speichern
	// ob der Character grade springt
	private bool isJumping = false; 


	// Start wird genau einmal am Spielstart von Unity aufgerufen, und ist damit ideal zum einmaligen 
	// initialisieren von Werten
	void Start () {
		character = gameObject.GetComponent<CharacterController>(); 
		moveVertical = new Vector3(0,0,0); 
	}
	

	void Update () {
		// Wie schon in den vorangegangen Beispielen lesen wir die Bewegung aus den Tasten-Werten aus ( siehe Robot3d script ) 
		// Damit der Character immer in die Richtung läuft in die er gedreht ist, multiplizieren wir die Geschwindigkeit 
		// mit der Laufrichtung ( transform.forward ), würden wir das weglassen könnte unser Character niemals schräg laufen 
		Vector3 moveHorizontal = MoveSpeed * transform.forward * Input.GetAxis("Vertical") ; 

		// Im gegensatz zum Robot3d benutzen wir die horizontale Achse um den Character zu drehen
		// und laufen nur noch vorwärts. Die Rotation soll immer um die Körperachse des Characters passieren
		// Deswegen die Multiplikation mit dieser Achse ( transform.up ) 
		Vector3 rotation = RotateSpeed * transform.up * Input.GetAxis("Horizontal") ; 

		// In dieser Unterfunktion berechnen wir Schwerkraft und Sprung 
		CalculateVerticalMove(); 

		// Die resultierende Bewegung unseres Characters ist die Summe der aufwärts / abwärts Bewegung mit den vorwärts / rückwärts Bewegung
		// Diese Addition hat den gleichen Effekt als wenn wir: 
		// character.Move( moveVertical ) ; und danach
		// character.Move( moveHorizontal ); aufrüfen würden 
		Vector3 move = moveVertical + moveHorizontal; 

		// Damit alle Bewegungen unabhögig von unserer PC geschwindigkeit sind ( Frames per Second ) 
		// multiplizieren wir den Wert mit der Zeit die seit dem letzten Update vergangen ist
		// haben wir eine sehr hohe FPS wird dieser wert klein, und damit auch der resultierende move wert ( dafür wird er ja aber öfter aufgerufen ) 
		// haben wir eine kleine FPS wird dieser wert größer, und damit auch der resultierende move wert ( dafür wird er ja aber seltener aufgerufen ) 
		character.Move ( move * Time.deltaTime) ; 
		transform.Rotate( rotation * Time.deltaTime) ;  

		// in dieser Unetrfunktion prüfen wir welche Animation abgespielt werden sollen 
		SetAnimations(move); 
	}

	private void CalculateVerticalMove(){
		// Character.isGrounded ist dann wahr, wenn der Character Boden kontakt hat
		if ( character.isGrounded ) {
			// Wir haben Bodenkontakt, also können wir springen
			if ( Input.GetAxis("Jump") == 1 ) {
				Jump(); 
			} else {
				isJumping = false; 
			}
		} else {
			// Wir haben keinen Bodenkontakt also addieren wir die Schwerkraft ( -9,81 ) auf 
			// unsere Abwärtsbewegenung, und mutliplieren mit deltaTime damit wir FPS unabhängig sind 
			moveVertical += Physics.gravity * Time.deltaTime; 
		}
	}

	private void SetAnimations(Vector3 move){
		// während des sprungs sind keine anderen Animationen erlaubt
		if ( !isJumping ) {
			// Idee: Um zu enscheiden ob wir die Walk animation abspielen müssen 
			// testen wir ob die Vorwärtsgeschwindigkeit des Characters ( z richtung ) > 0 ist 
			if ( move.z != 0) 
				// Im Animator haben wir die Checkbox ( bool ) "IsWalking" eingestellt ( siehe README ) 
				// diese Checkbox schalten wir jetzt an, da sich unser Character vorwärts bewegt
				animator.SetBool("IsWalking", true ) ; 
			else  {
				// bzw. ab, wenn wir uns nicht vorwärts bewegen 
				animator.SetBool("IsWalking", false); 
			}
		} 
		animator.SetBool( "IsJumping", isJumping ) ; 
	}

	private void Jump(){
		// Jump bedeutet eine apprupte änderung der Bewegung in der Aufwärtsbewegung ( y achse ) 
		moveVertical.y = JumpPower; 
		isJumping = true;  
	}

	public void CollectGoodie(){
		// Jump Animation kann man auch als Happy sehen :) 
		Jump(); 
	}
}
