﻿using UnityEngine;
using System.Collections;

public class Robot3d : MonoBehaviour {

	// Variablen die außerhalb der Funktionen erstellt werden 
	// sind in allen Funktionen des Robot3d scripts verfügbar 
	// Public variablen sorgen dafür das wir sie im Inspector in Unity 
	// einstellen können, der Wert der hier eingestellt ist ( 1 ) wird dann 
	// nicht mehr beachtet
	public float speed = 1; 
	
	// Update wird jedes Frame ( Bildwiederholung ) von Unity aufgerufen 
	// normalerweise 60 mal pro Sekunde 
	void Update () {
		// Wir testen ob der Player eine der Achsentasten gedrückt hat
		CheckAxis(); 
	}

	private void CheckAxis(){
		// Die Achse "Horizontal" ist eingestellt in den Inputsettings von Unity 
		// Edit -> ProjectSettings -> Input 
		// Der Wert der Achse liegt zwischen -1 ( negative Taste, 0 gar nicht gedrückt, 1 positive taste ) 
		// Beispiel: Pfeil Links ( negativ Taste ) , Pfeil Rechts positive Taste
		float horizontalInput = Input.GetAxis("Horizontal"); 

		// damit sich unser Character nicht immer nur mit 1 vorwärts bewegt multiplizieren wir den Wert der Taste 
		// mit unserem speed 
		// Wir bewegen den Character im 3d raum deswegen Vector3 als typ wobei x die seitwärts bewegung ist
		// vorwärst ( z ) und abwärts ( y ) lassen wir auf 0 
		Vector3 moveDirection = new Vector3(horizontalInput*speed,0,0); 
		MoveRobotWithForces( moveDirection ) ; 

		// und das gleiche noch mal für die Verticale Achse 
		float verticalInput = Input.GetAxis("Vertical"); 
		moveDirection = new Vector3(0,0,verticalInput*speed); 
		MoveRobotWithForces( moveDirection ) ; 
	}

	void MoveRobotWithForces(Vector3 positionChange){
		// Die Idee hinter dieser Bewegungsvariante ist das wir den Character durch das anwenden von 
		// Physikalischenkräften herum schieben
		// Phisik kann nur auf Rigidbodys angewendent werden. Den holen wir uns nach bekannter manier mit GetComponent
		var player = GetComponent<Rigidbody>(); 

		// Damit sich die Kräfte nich "hochschaukeln" also unser Character immer schneller wird setzen wir 
		// die aktuelle geschwindigkeit auf 0 ( in allen Richtungen ) 
		player.velocity = new Vector3( 0, 0, 0 ) ; 

		// und "schubsen"  den Character jetzt nach vorne, wieder multiplieziert mit unserem Speed
		player.AddForce( positionChange*speed ) ; 
	}

	/// <summary>
	/// Diese Funktion wird von Goodie3d aufgerufen, wenn der Character in das Goodie läuft und damit
	/// die Funktion OnTriggerEnter auslöst.
	/// </summary>
	public void CollectGoodie(){
		// erstmal schreiben wir nur eine Zeile in die Console
		// später könnte man hier je nach Spiel Punkte zählen o.ä
		Debug.Log("Yippih I got stuff");
	}
}
