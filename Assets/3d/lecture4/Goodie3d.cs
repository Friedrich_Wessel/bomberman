﻿using UnityEngine;
using System.Collections;

public class Goodie3d : MonoBehaviour {

	/// <summary>
	/// Dieses Script wird von Unity aufgerufen wenn der BoxCollider von dem Object "Player"
	/// Mit dem Boxcollider von diesem Goodie kollidiert
	/// Der Collider der übergeben wird, ist das andere Boxcollider ( also der vom Player ) 
	/// </summary>
	void OnTriggerEnter(Collider collider){
		// wir wissen das der andere Collider der Robot ist, also holen 
		// wir uns die Komponente von diesem Objekt
		Robot3d robot = collider.GetComponent<Robot3d>(); 
		// Danach können die in Robot3d existierende Funktion CollectGoodie aufrufen 
		robot.CollectGoodie(); 

		// und das Goodie zertören
		// this.gameOject bezieht sich immer auf das Object auf dem das Script exisitiert
		Destroy( this.gameObject ) ; 
	}
}
