﻿Lecture 4 

Diese Datei beschreibt den generellen Scenen Aufbau. Die Programmierung ist mit einzelnen Kommentaren an den Zeilen 
in den Script beschrieben. 
Anmerkung zu Mono und den Kommentaren: 
Links am Rand der Scripte ( da wo sich die Zeilennummern befinden ) sind manchmal kleine + zeichen zu sehen 
damit lassen sich kommentar blöcke ausklappen 

In der Scenen befinden sich der Würfel namens Player. 
Dieser kann wie folgt erstellt werden: 
* Create -> 3dObject -> Cube
* Den Cube auswählen und "Player" nennen 
* Player in der Hirachy selektieren, rechts im Inspector mit dem "AddComponent" Button folgende Elemente hinzufügen : 
	* Boxxcollider3d ( der existiert normalerweise schon - vorher checken ) 
		* Der BoxCollider muss im Material Slot den Material "NoFriction" verknüpft haben
	* Rigidbody 
	* Robot3d
* Das Robot Script steuert die Bewegung über die Tasten 
* Wenn sich der Roboter nicht bewegt ist wahrscheinlich der Speed zu klein ( Robot Componente ) 
	* ein Wert von 30 funktioniert gut 
	
Goodie kann erstellt werden : 
* Create -> 3dObject -> Cube 
* Den Cube "Goodie" nennen 
* Über den AddComponent Button folgendes Element hinzufügen : 
	* Goodie3d 
	* BoxCollider3d ( existiert wahrschienlich schon ) 
	* Auf dem BoxCollider den Haken bei "IsTrigger" setzen
		* Dadurch wird auf unserem Goodie Script die OnTriggerEnter aufgerufen wenn der Spieler mit der Box Kollidiert
	

